archive
===

`archive` is a simple helper for working with tar.gz archives.  The notable
features are:

- Usage of pgzip.  Under the correct set of circumstances, this makes compression
  **much** faster due to being able to better utilize multi-core systems.
- Easily create basic archives.  "Advanced" features such as excluding certain
  directories are not (currently?) included.
- Encrypt the archives during creation.  This is done using AES-256-CTR using a
  randomly generated per-archive key and IV.
- Decrypt an encrypted archive.

## Usage

To create an encrypted archive:

```go
import "bitbucket.org/timewasted/archive"

file, err := archive.New("file.tar.gz.enc")
if err != nil {
        panic(err)
}
defer file.Close()
if err = file.Encrypt(); err != nil {
        panic(err)
}

if err = file.AddPath("./compress-this-dir"); err != nil {
        panic(err)
}

key, err := file.Key()
if err != nil {
        panic(err)
}
// Do something with the key.
```

To later decrypt an encrypted archive:

```go
import "bitbucket.org/timewasted/archive"

// Get the key that you no doubt stored when encrypting the archive.
if err := archive.Decrypt("file.tar.gz.enc", "file.tar.gz", key); err != nil {
        panic(err)
}
// You now have a decrypted archive to work with.
```

Decryption does not modify/delete the encrypted archive.
