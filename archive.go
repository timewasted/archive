// Package archive is a simple helper for creating (optionally encrypted) tar.gz
// archives.
package archive

import (
	"archive/tar"
	"crypto/aes"
	"crypto/cipher"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"

	gzip "github.com/klauspost/pgzip"
)

// COMPRESSION_LEVEL is the numeric compression level to use.
const COMPRESSION_LEVEL = 8

const (
	// KEY_SIZE is the size of the encryption key to use, in bytes.
	KEY_SIZE = 32
	// IV_SIZE is the size of the encryption IV to use, in bytes.
	IV_SIZE = aes.BlockSize
)

// Archive represents an archive that can be used for writing.
type Archive struct {
	key          []byte
	iv           []byte
	cryptoWriter cipher.StreamWriter
	gzipWriter   *gzip.Writer
	tarWriter    *tar.Writer
	outputFile   *os.File
	initialized  bool
}

// Decrypt reads an encrypted file and writes out a decrypted file.
func Decrypt(input string, output string, key []byte) (err error) {
	// Open the input file and pull the IV out of it.
	inputFile, fileInfo, iv, err := openEncryptedArchive(input)
	if err != nil {
		return
	}
	defer inputFile.Close()

	// Initialize the encryption.
	block, err := aes.NewCipher(key)
	if err != nil {
		return
	}
	stream := cipher.NewCTR(block, iv)

	// Create the output file for writing.
	outputFile, err := os.Create(output)
	if err != nil {
		return
	}
	defer outputFile.Close()

	// Decrypt the archive.
	cryptoReader := cipher.StreamReader{
		S: stream,
		R: inputFile,
	}
	n, err := io.Copy(outputFile, cryptoReader)
	if err != nil {
		return
	}
	if n != fileInfo.Size() {
		return errors.New(fmt.Sprintf("archive: expected to read %d bytes, instead read %d.", fileInfo.Size(), n))
	}

	return
}

// New creates a new archive that can be worked with.
func New(output string) (a *Archive, err error) {
	file, err := os.Create(output)
	if err != nil {
		return
	}

	a = &Archive{outputFile: file}
	return
}

// Encrypt will generate a random key and IV to be used for encryption while
// writing the archive.  Note that this can NOT be called after adding files to
// the archive!
func (a *Archive) Encrypt() error {
	if a.initialized {
		return errors.New("archive: unable to toggle encryption for an archive that has already been initialized.")
	}

	a.key = randomBytes(KEY_SIZE)
	a.iv = randomBytes(IV_SIZE)

	if !a.initialized {
		if err := a.initialize(); err != nil {
			return err
		}
	}
	n, err := a.outputFile.Write(a.iv)
	if err != nil {
		return err
	}
	if n != IV_SIZE {
		return errors.New(fmt.Sprintf("archive: expected to write %d IV bytes, instead wrote %d.", IV_SIZE, n))
	}

	return nil
}

// Close closes the archive, flushing all data to disk and preventing further
// actions from being taken.
func (a *Archive) Close() {
	if !a.initialized {
		return
	}

	a.tarWriter.Close()
	a.gzipWriter.Close()
	if a.cryptoWriter.W != nil {
		a.cryptoWriter.Close()
	}
	a.outputFile.Close()

	a.initialized = false
}

// Key returns the encryption key being used for this archive, if encryption
// has been enabled.
func (a *Archive) Key() ([]byte, error) {
	if len(a.key) != KEY_SIZE && len(a.iv) != IV_SIZE {
		return []byte{}, errors.New("archive: encryption is not enabled for this archive.")
	}
	return a.key, nil
}

// AddPath adds the specified path to the archive.  If the path is a directory,
// it will also add all children to the archive.
func (a *Archive) AddPath(path string, ignorePatterns ...string) error {
	// Maintain a list of directories that have matched an ignored pattern.
	var matchedDirs []string

	return filepath.Walk(path, func(filePath string, fileInfo os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// Check to see if we're in a directory that we have previously
		// matched.  If so, skip processing it.
		if len(matchedDirs) != 0 {
			for _, dir := range matchedDirs {
				if strings.HasPrefix(filePath, dir) {
					return nil
				}
			}
		}

		// Check to see if we're processing a resource that matches a
		// pattern that we wish to ignore.  If it matches, skip it.  In
		// addition, if it's a directory, add it to the list of matched
		// directories so that we can easily skip all subdirectories.
		if len(ignorePatterns) != 0 {
			for _, pattern := range ignorePatterns {
				match, err := filepath.Match(pattern, filePath)
				if err != nil {
					return err
				}
				if match {
					if fileInfo.IsDir() {
						matchedDirs = append(matchedDirs, filePath)
					}
					return nil
				}
			}
		}

		// Write the tar header.
		header, err := tar.FileInfoHeader(fileInfo, fileInfo.Name())
		if err != nil {
			return err
		}
		header.Name = filepath.Join(path, strings.TrimPrefix(filePath, path))
		if !a.initialized {
			if err = a.initialize(); err != nil {
				return err
			}
		}
		if err = a.tarWriter.WriteHeader(header); err != nil {
			return err
		}

		// Directories require no further processing.
		if fileInfo.IsDir() {
			return nil
		}

		// Add the file to the archive.
		file, err := os.Open(filePath)
		if err != nil {
			return err
		}
		defer file.Close()
		n, err := io.Copy(a.tarWriter, file)
		if err != nil {
			return err
		}
		if n != fileInfo.Size() {
			return errors.New(fmt.Sprintf("archive: expected to write %d bytes, instead wrote %d.", fileInfo.Size(), n))
		}

		return nil
	})
}

// initialize will initialize the archive so that writing operations can take
// place.
func (a *Archive) initialize() (err error) {
	if a.initialized {
		return
	}
	a.initialized = true

	var baseWriter io.Writer
	if len(a.key) != 0 && len(a.iv) != 0 {
		block, err := aes.NewCipher(a.key)
		if err != nil {
			panic(err)
		}
		stream := cipher.NewCTR(block, a.iv)
		a.cryptoWriter = cipher.StreamWriter{
			S: stream,
			W: a.outputFile,
		}
		baseWriter = a.cryptoWriter
	} else {
		baseWriter = a.outputFile
	}
	a.gzipWriter, err = gzip.NewWriterLevel(baseWriter, COMPRESSION_LEVEL)
	if err != nil {
		return
	}
	a.tarWriter = tar.NewWriter(a.gzipWriter)

	return nil
}
