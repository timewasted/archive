package archive

import (
	"crypto/rand"
	"errors"
	"fmt"
	"os"
)

// randomBytes reads the specified number of bytes (greater than zero), and
// panics if it can't read that many bytes.
func randomBytes(len int) []byte {
	if len <= 0 {
		panic("archive: length must be greater than zero.")
	}
	output := make([]byte, len)
	n, err := rand.Read(output)
	if n != len {
		panic(fmt.Sprintf("archive: expected to read %d random bytes, instead read %d.\n", len, n))
	}
	if err != nil {
		panic(err)
	}
	return output
}

// openEncryptedArchive will open the specified path, and if it looks to be a
// valid encrypted archive, return information required to work with the
// encrypted archive.
func openEncryptedArchive(path string) (file *os.File, fileInfo os.FileInfo, iv []byte, err error) {
	// Verify that the specified file exists and has a length greater than
	// the IV size.
	fileInfo, err = os.Stat(path)
	if err != nil {
		return
	}
	if fileInfo.Size() <= IV_SIZE {
		err = errors.New(fmt.Sprintf("archive: encrypted archives must be greater than %d bytes.", IV_SIZE))
		return
	}

	// Open the file, and pull the IV out.
	file, err = os.Open(path)
	if err != nil {
		return
	}
	iv = make([]byte, IV_SIZE)
	n, err := file.Read(iv)
	if err != nil {
		_ = file.Close()
		return
	}
	if n != IV_SIZE {
		_ = file.Close()
		err = errors.New(fmt.Sprintf("archive: expected to read %d IV bytes, instead read %d.", IV_SIZE, n))
		return
	}

	return
}
